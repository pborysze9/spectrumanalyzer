#include "audiorecordworker.h"
#include <QDebug>
#include <QAbstractEventDispatcher>

static constexpr int sampleMaxValue = 1<<15;

void AudioRecordWorker::captureSamples(int samplingRate,int numberOfSamples,QString windowType,bool isLinearAxis){
    qDebug()<<"Start recording";
    qDebug()<<"Window ="<<windowType;
    stopFlag = false;
    auto windowFn = FftWindows::getWindowFunctionFromString(windowType);
    AudioCapture audioCapture(4*numberOfSamples, samplingRate);
    QVector<ALshort> tempBuffer(2*numberOfSamples);
    QVector<double> samples(numberOfSamples);
    audioCapture.startRecording();
    while(!stopFlag){
        if(audioCapture.availableSamples()>=numberOfSamples){
            audioCapture.getSamples(tempBuffer.data(),numberOfSamples);
            for(int i=0;i<samples.size();i++){
                samples[i] = windowFn(i,numberOfSamples)*((double)tempBuffer[i])/(double)sampleMaxValue;
            }
            emit samplesReady(samples, isLinearAxis);
        }
        QThread::currentThread()->eventDispatcher()->processEvents(QEventLoop::AllEvents);
    }
    qDebug()<<"Stop recording";
    audioCapture.stopRecording();
}
void AudioRecordWorker::stop(){
    qDebug()<<"In audiorecordworker stop";
    stopFlag = true;
}
