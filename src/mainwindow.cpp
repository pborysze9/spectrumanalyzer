#include "mainwindow.h"
#include "ui_mainwindow.h"

const int sampleRate = 44100;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    numberOfSamples = 512;
    samplingRate = 44100;
    threadsStopped = true;
    setN();
    windowType = "Rectangular";
    isLinearAxis = true;
    numOfSamplesList<<"512"<<"1024"<<"2048"<<"4096"<<"8192";
    ui->numberOfSamplesComboBox->addItems(numOfSamplesList);
    windowsTypes<<"Rectangular"<<"Triangle"<<"Hamming"<<"Blackman";
    ui->windowTypeComboBox->addItems(windowsTypes);
    axisTypes<<"Linear"<<"Logarithmic";
    ui->axisTypeComboBox->addItems(axisTypes);
    initPlot();
    //ui connection
    connect(ui->numberOfSamplesComboBox, QOverload<int>::of(&QComboBox::activated), [this](int index) { this->numberOfSamples = numOfSamplesList[index].toInt(); setN(); });
    connect(ui->windowTypeComboBox, QOverload<int>::of(&QComboBox::activated), [this](int index) { this->windowType = windowsTypes[index]; });
    connect(ui->samplingRateSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), [this](int i) { this->samplingRate = i; } );
    connect(ui->axisTypeComboBox, QOverload<int>::of(&QComboBox::activated), this, &MainWindow::axisChanged);
    //threads
    AudioRecordWorker* audioRecordWorker = new AudioRecordWorker;
    FFTWorker* fftWorker = new FFTWorker;
    audioRecordWorker->moveToThread(&audioThread);
    fftWorker->moveToThread(&fftThread);
    connect(&fftThread, SIGNAL(finished()), fftWorker, SLOT(deleteLater()));
    connect(&audioThread, SIGNAL(finished()), audioRecordWorker, SLOT(deleteLater()));
    connect(audioRecordWorker, &AudioRecordWorker::samplesReady, fftWorker, &FFTWorker::computeFFT);
    connect(fftWorker, SIGNAL(fftReady(QVector<double>)), this, SLOT(plotSamples(QVector<double>)));
    //buttons
    connect(ui->startButton, &QPushButton::clicked, this, &MainWindow::trigRecording);
    connect(this, &MainWindow::startRecording, audioRecordWorker, &AudioRecordWorker::captureSamples);
    connect(this, SIGNAL(stopRecording()), audioRecordWorker, SLOT(stop()));
    connect(this, &MainWindow::stopRecording, fftWorker, &FFTWorker::clean);
    connect(ui->stopButton, SIGNAL(clicked()), this, SLOT(onStopButtonClick()));
    //removing threads and cleaning
    connect(&fftThread, SIGNAL(finished()), &fftThread, SLOT(deleteLater()));
    connect(&audioThread, SIGNAL(finished()), &audioThread, SLOT(deleteLater()));
    audioThread.start();
    fftThread.start();
}
void MainWindow::axisChanged(int index){
    if(axisTypes[index]=="Logarithmic"){
        isLinearAxis = false;
        ui->audioPlot->yAxis->setRange(-120, 0);
    }else{
        ui->audioPlot->yAxis->setRange(0, 1);
        isLinearAxis = true;
    }
}
void MainWindow::initPlot(){
    ui->audioPlot->addGraph();
    ui->audioPlot->xAxis->setLabel("Frequency");
    ui->audioPlot->yAxis->setLabel("Normalized magnitude");
    ui->audioPlot->yAxis->setRange(0, 1);
    ui->audioPlot->graph(0)->setPen(QPen(Qt::blue, 3));
}
void MainWindow::trigRecording(){
    ui->startButton->setDisabled(true);
    ui->stopButton->setDisabled(false);
    ui->samplingRateSpinBox->setDisabled(true);
    ui->numberOfSamplesComboBox->setDisabled(true);
    ui->windowTypeComboBox->setDisabled(true);
    ui->axisTypeComboBox->setDisabled(true);
    threadsStopped = false;
    emit startRecording(samplingRate, numberOfSamples, windowType, isLinearAxis);
}
void MainWindow::onStopButtonClick(){
    ui->startButton->setDisabled(false);
    ui->stopButton->setDisabled(true);
    ui->samplingRateSpinBox->setDisabled(false);
    ui->numberOfSamplesComboBox->setDisabled(false);
    ui->windowTypeComboBox->setDisabled(false);
    ui->axisTypeComboBox->setDisabled(false);
    threadsStopped = true;
    emit stopRecording();
}
void MainWindow::setN(){
    n.resize(numberOfSamples/2+1);
    for(int i=0;i<n.size();i++){
        n[i] = i*samplingRate/numberOfSamples;
    }
}
void MainWindow::plotSamples(QVector<double> samples){
    ui->audioPlot->xAxis->setRange(0, samplingRate/2);
    ui->audioPlot->graph(0)->setData(n, samples);
    ui->audioPlot->replot();
}
void MainWindow::closeEvent(QCloseEvent* event){
    if(!threadsStopped){
        emit stopRecording();
    }
    event->accept();
}

MainWindow::~MainWindow(){   
    audioThread.quit();
    fftThread.quit();
    fftThread.wait();
    audioThread.wait();
    delete ui;
}

