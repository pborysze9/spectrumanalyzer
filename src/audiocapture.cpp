#include "audiocapture.h"

AudioCapture::AudioCapture(int buffer_size,int sample_rate){
    errorCode=0;
    audioDevice = alcOpenDevice(NULL);
    if(audioDevice==NULL){
        throw "OpenAl: cannot open device";
    }
    audioContext = alcCreateContext(audioDevice,NULL);
    alcMakeContextCurrent(audioContext);
    if((errorCode = alcGetError(audioDevice))!=AL_NO_ERROR){
          std::string e = "OpenAl error with code"+std::to_string((int)errorCode)+" when making context";
          throw e;
    }
    inputDevice = alcCaptureOpenDevice(NULL,sample_rate,AL_FORMAT_MONO16,buffer_size);
    if((errorCode = alcGetError(inputDevice))!=AL_NO_ERROR){
        std::string e = "OpenAl error with code"+std::to_string((int)errorCode)+" when opening device";
        throw e;
    }
}
void AudioCapture::startRecording()
{
    alcCaptureStart(inputDevice); // Begin capturing
    if((errorCode = alcGetError(inputDevice))!=AL_NO_ERROR){
        std::string e = "OpenAl error with code"+std::to_string((int)errorCode)+" when starting capture";
        throw e;
    }
}
void AudioCapture::stopRecording()
{
    alcCaptureStop(inputDevice);
}
ALint AudioCapture::availableSamples(){
    ALint sample=0;
    alcGetIntegerv(inputDevice, ALC_CAPTURE_SAMPLES, (ALCsizei)sizeof(ALint), &sample);//return number of bytes not two bytes samples
    return sample/2;
}
void AudioCapture::getSamples(ALshort* buff,ALint number_of_samples)
{
    alcCaptureSamples(inputDevice, (ALCvoid *)buff, number_of_samples*2);//number of samples multiply by 2 beacuse every sample has 2 bytes
}
AudioCapture::~AudioCapture()
{
    alcCaptureCloseDevice(inputDevice);
    alcMakeContextCurrent(NULL);
    alcDestroyContext(audioContext);
    alcCloseDevice(audioDevice);
}
