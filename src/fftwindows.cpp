#include "fftwindows.h"

double FftWindows::hamming(unsigned int n, unsigned int length){
    return 0.54 - 0.46*cos(2*M_PI*n/(length-1));
}
double FftWindows::blackman(unsigned int n, unsigned int length){
    return 0.42 - cos(2*M_PI*n/length) + 0.08*cos(4*M_PI*n/length);
}
double FftWindows::triangle(unsigned int n, unsigned int length){
    return n <=(length/2) ? 2*n/length : (2-2*n/length);
}
double FftWindows::rectangular(unsigned int n, unsigned int length){
    return 1.0;
}

FftWindows::windowFunction FftWindows::getWindowFunctionFromString(QString window){
    if(window=="Rectangular"){
        return rectangular;
    }else if(window=="Hamming"){
        return hamming;
    }else if(window=="Blackman"){
        return blackman;
    }else if(window=="Triangle"){
       return triangle;
    }else{
        throw std::invalid_argument("Window with this name not exist!!!");
    }
}
