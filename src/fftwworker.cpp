#include "fftwworker.h"

static constexpr double logHelper = 20.0*0.301029996;

FFTWorker::FFTWorker(){
    numberOfSamples = 0;
    isDeleted = true;
}

void FFTWorker::computeFFT(QVector<double> inputData,bool isLinearAxis){
    isDeleted = false;
    if(numberOfSamples!=inputData.size()){
        numberOfSamples = inputData.size();
        result.resize(numberOfSamples/2+1);
        input_fftw = (double*)fftw_malloc(numberOfSamples*sizeof(double));
        memcpy(input_fftw, inputData.data(), numberOfSamples*sizeof(double));
        out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*(numberOfSamples/2+1));
    }
    memcpy(input_fftw, inputData.data(), numberOfSamples*sizeof(double));
    my_plan = fftw_plan_dft_r2c_1d(numberOfSamples, input_fftw, out, FFTW_ESTIMATE);
    fftw_execute(my_plan);
    if(isLinearAxis){
        for(int i=0;i<(numberOfSamples/2+1);i++){
            result[i] = std::abs(std::complex<double>(out[i][0],out[i][1]))*2/numberOfSamples;//linear
    }
    }else{
        for(int i=0;i<(numberOfSamples/2+1);i++){
            result[i] = logHelper + 20.0*log10(std::abs(std::complex<double>(out[i][0],out[i][1]))/(double)numberOfSamples);//logarithmic
        }
    }
    emit fftReady(result);
}
void FFTWorker::clean(){
    qDebug()<<"Cleaning";
    fftw_destroy_plan(my_plan);
    fftw_free(input_fftw);
    fftw_free(out);
    numberOfSamples = 0;
    isDeleted = true;
}
FFTWorker::~FFTWorker(){
    if(!isDeleted){
        fftw_destroy_plan(my_plan);
        fftw_free(input_fftw);
        fftw_free(out);
    }
}
