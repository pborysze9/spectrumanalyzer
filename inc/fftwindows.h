#ifndef FFTWINDOWS_H
#define FFTWINDOWS_H

#include <cmath>
#include <string>
#include <QString>
#include <stdexcept>

namespace FftWindows{

    typedef double (*windowFunction)(unsigned int, unsigned int);

    windowFunction getWindowFunctionFromString(QString window);
    inline double hamming(unsigned int n, unsigned int length);
    inline double blackman(unsigned int  n, unsigned int length);
    inline double triangle(unsigned int n, unsigned int length);
    inline double rectangular(unsigned int n, unsigned int length);
};
#endif // FFTWINDOWS_H
