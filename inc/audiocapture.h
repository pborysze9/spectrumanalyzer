#include <AL/al.h>
#include <AL/alc.h>
#include <cstdlib>
#include <stdexcept>
#include <string>

class AudioCapture
{
    private:
    	ALenum errorCode;
    	ALCchar* available_devices;
    	ALCdevice* audioDevice;
    	ALCcontext* audioContext;
    	ALCdevice* inputDevice;
    public:
        AudioCapture(int buffer_size,int sample_rate=44100);
        ~AudioCapture();
    	void startRecording();
    	void stopRecording();
        void getSamples(ALshort* buff,ALint number_of_samples);
        ALint availableSamples();
};
