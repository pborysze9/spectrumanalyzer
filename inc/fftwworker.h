#ifndef FFTWWORKER_H
#define FFTWWORKER_H

#include <fftw3.h>
#include <complex>
#include <QThread>
#include <QVector>
#include <cstring>
#include <cmath>
#include <QDebug>
#include <algorithm>

class FFTWorker : public QObject {
    Q_OBJECT
    private:
        bool isDeleted;
        int numberOfSamples;
        QVector<double> result;
        double* input_fftw;
        fftw_complex* out;
        fftw_plan my_plan;
    public:
        FFTWorker();
        ~FFTWorker();
    public slots:
        void computeFFT(QVector<double> inputData,bool isLinearAxis);
        void clean();
    signals:
        void fftReady(QVector<double> fftData);
};


#endif // FFTWWORKER_H
