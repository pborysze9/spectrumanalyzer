#ifndef AUDIORECORDWORKER_H
#define AUDIORECORDWORKER_H

#include <QVector>
#include <QThread>
#include <QString>
#include <audiocapture.h>
#include <fftwindows.h>

class AudioRecordWorker : public QObject {
  Q_OBJECT
    private:
        bool stopFlag;
    public:
        AudioRecordWorker() : QObject() { stopFlag = false; };
    public slots:
        void captureSamples(int samplingRate,int numberOfSamples,QString windowType,bool isLinearAxis);
        void stop();
    signals:
        void samplesReady(QVector<double> samples, bool isLinearAxis);
};

#endif // AUDIORECORDWORKER_H
