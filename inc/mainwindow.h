#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QString>
#include <QThread>
#include <QStringList>
#include <audiorecordworker.h>
#include <fftwworker.h>
#include <QCloseEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void closeEvent(QCloseEvent *event);
public slots:
    void axisChanged(int index);
private slots:
    void plotSamples(QVector<double> samples);
    void trigRecording();
    void onStopButtonClick();
signals:
    void startRecording(int samplingRate,int numberOfSamples,QString window,bool _isLinearAxiss);
    void stopRecording();
private:
    int numberOfSamples;
    int samplingRate;
    bool threadsStopped;
    bool isLinearAxis;
    QString windowType;
    QVector<double> n;
    QStringList windowsTypes;
    QStringList numOfSamplesList;
    QStringList axisTypes;
    QThread audioThread;
    QThread fftThread;
    Ui::MainWindow *ui;
    void initPlot();
    void setN();
};
#endif // MAINWINDOW_H
